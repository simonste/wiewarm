package ch.simonste.wiewarm;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import ch.simonste.wiewarm.WieWarm.BadInfo;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link ListFramentListener}
 * interface.
 */
public class ListFragment extends Fragment implements ListView.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener, WieWarmWrapper.WieWarmCaller {

    private ListFramentListener mListener;

    private SwipeRefreshLayout mRootview;
    private ListView mListView;
    private FavoritesAdapter mAdapter;
    private String query = "";

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAdapter = new FavoritesAdapter(getActivity(), new ArrayList<BadInfo>());

        loadFavorites();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootview = (SwipeRefreshLayout) inflater.inflate(R.layout.fragment_list, container, false);
        mRootview.setOnRefreshListener(this);

        // Set the adapter
        mListView = (ListView) mRootview.findViewById(android.R.id.list);
        mListView.setAdapter(mAdapter);

        // FIXME: empty view not working
        mListView.setEmptyView(mRootview.findViewById(android.R.id.empty));

        mListView.setOnItemClickListener(this);


        return mRootview;
    }

    @Override
    public void onRefresh() {
        loadFavorites();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (ListFramentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {
            mListener.showDetails(mAdapter.getItem(position));
        }
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyView instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }

    public void loadFavorites() {
        mAdapter.clear();
        for (int fav : mAdapter.getFavorites()) {
            new WieWarmWrapper().getBath(fav, this);
        }
        if (!mAdapter.getFavorites().isEmpty() && mRootview != null) {
            mRootview.setRefreshing(true);
        }
    }

    public void searchBaths(String query) {
        mAdapter.clear();
        if (!this.query.equals(query)) {
            new WieWarmWrapper().searchBaths(query, this);
            this.query = query;
        }
        if (mRootview != null) mRootview.setRefreshing(true);
    }

    @Override
    public void gotBad(BadInfo bad) {
        boolean newbad = true;
        for (int i = 0; i < mAdapter.getCount(); i++) {
            if (mAdapter.getItem(i).id == bad.id) newbad = false;
        }
        if (newbad) {
            mAdapter.add(bad);
            mAdapter.sort();
        }
        if (mRootview != null) mRootview.setRefreshing(false);
    }

    @Override
    public void gotBaeder(List<BadInfo> baeder) {
        mAdapter.addAll(baeder);

        if (baeder.size() == 0) {
            // mListView.addHeaderView(mRootview.findViewById(android.R.id.empty));
            mAdapter.notifyDataSetChanged();
            // Log.d("STS", "nothing found");
        }
        if (mRootview != null) mRootview.setRefreshing(false);
    }

    public interface ListFramentListener {
        void showDetails(BadInfo bad);
    }
}
