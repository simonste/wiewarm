package ch.simonste.wiewarm.WieWarm;

import android.content.res.Resources;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ch.simonste.wiewarm.R;

public class BeckenInfo {
    private static final String TAG = "Becken";
    public double temp;
    public Date date;
    public String beckenname;
    public BeckenTyp typ;
    public boolean offen;
    public boolean valid = true;

    BeckenInfo(JSONObject json) {
        try {
            this.beckenname = json.getString("beckenname");
            this.temp = json.getDouble("temp");
            this.offen = json.getString("status") == "geöffnet";
            try {
                this.typ = BeckenTyp.valueOf(json.getString("typ"));
            } catch (IllegalArgumentException e) {
                this.typ = BeckenTyp.None;
            }
            try {
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                date = df.parse(json.getString("date"));
            } catch (ParseException e) {
                Log.e(TAG, e.getMessage());
            }
        } catch (JSONException e) {
            Log.e(TAG, "Json exception " + e.getMessage());
            this.valid = false;
        }
    }

    public String getLastUpdateTime() {
        if (date == null) return Resources.getSystem().getString(R.string.neverupdated);
        Date now = new Date();
        int days = (int) Math.floor((now.getTime() - date.getTime()) / (1000 * 60 * 60 * 24));
        int hours = (int) Math.floor((now.getTime() - date.getTime()) / (1000 * 60 * 60) - days * 24);

        String ret = Resources.getSystem().getString(R.string.lastupdate);
        if (days > 0) ret += Resources.getSystem().getQuantityString(R.plurals.days, days, days);
        if (days < 4) ret += Resources.getSystem().getQuantityString(R.plurals.hours, hours, hours);
        return ret;
    }


    public enum BeckenTyp {
        Fluss,
        See,
        Hallenbad,
        Freibad,
        Sole,
        None
    }
}