package ch.simonste.wiewarm.WieWarm;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BadInfo {
    private static final String TAG = "Becken";
    public final int id;
    public final String badname;
    public final String ort;
    public final String kanton;
    public final String adresse;
    public final String email;
    public final String telefon;
    public final String homepage;
    public final String zeiten;
    public final String preise;
    public final String info;
    public List<BeckenInfo> becken = new ArrayList<>();
    public List<Picture> pictures = new ArrayList<>();
    public List<News> news = new ArrayList<>();
    public List<Weather> weather = new ArrayList<>();

    public BadInfo(String json) throws JSONException {
        JSONObject object = new JSONObject(json);

        id = object.getInt("badid");
        badname = object.getString("badname");
        ort = object.getString("ort");
        kanton = object.getString("kanton");
        adresse = object.getString("adresse1") + "\n" + object.getString("adresse2") +
                "\n" + object.getString("plz") + " " + ort;

        String[] data = new String[6];
        try {
            // These values are only available in id search
            data[0] = object.getString("email");
            data[1] = object.getString("telefon");
            data[2] = object.getString("www");
            data[3] = object.getString("zeiten");
            data[4] = object.getString("preise");
            data[5] = object.getString("info");

            JSONArray picturesList = object.getJSONArray("bilder");
            for (int i = 0; i < picturesList.length(); i++) {
                JSONObject pic = picturesList.getJSONObject(i);
                pictures.add(new Picture("https://www.wiewarm.ch/" + pic.getString("image"), pic.getString("text")));
            }
        } catch (JSONException ignored) {
        }

        try {
            JSONArray newsList = object.getJSONArray("infos");
            for (int i = 0; i < newsList.length(); i++) {
                JSONObject pic = newsList.getJSONObject(i);
                news.add(new News(pic.getString("date_pretty"), pic.getString("info")));
            }
        } catch (JSONException ignored) {
        }

        email = data[0];
        telefon = data[1];
        homepage = data[2];
        zeiten = data[3];
        preise = data[4];
        info = data[5];

        Log.i(TAG, "Recieved: " + badname + "," + ort);
        JSONObject beckenList = object.getJSONObject("becken");
        Iterator<?> keys = beckenList.keys();
        while (keys.hasNext()) {
            JSONObject beckenStr = beckenList.getJSONObject((String) keys.next());
            BeckenInfo beckenInfo = new BeckenInfo(beckenStr);
            if (beckenInfo.valid)
                becken.add(beckenInfo);
        }
    }

    public boolean equals(BadInfo b) {
        return b.id == this.id;
    }

    public class Picture {
        public String url;
        public String description;

        Picture(String url, String description) {
            this.url = url;
            this.description = description;
        }
    }

    public class News {
        public String date;
        public String text;

        News(String date, String text) {
            this.date = date;
            this.text = text;
        }
    }
/*
    "badid": 210,
            "badname": "Schwimmbad",
            "kanton": "AR",
            "plz": "9410",
            "ort": "Heiden",
            "adresse1": "Schwimmbad Heiden",
            "adresse2": "Kohlplatz 5",
            "email": "schwimmbad@heiden.ar.ch",
            "telefon": "071 891 12 23",
            "www": null,
            "long": null,
            "lat": null,
            "zeiten": "Öffnungszeiten:\nVorsaison: 14. Mai bis 05. Juni 2015\nHauptsaison: 06. Juni bis 9. August 2015\nNachsaison: 10. August bis 06. September (ev. 13.September) 2015 \n \n09.00  19.00 Uhr bei schönem Wetter (Hauptsaison bis 20.00)\n09.00  11.00 Uhr bei jeder Witterung täglich\n15.30  19.00 Uhr bei jeder Witterung am Mittwoch\n\nFrühschwimmen: 07.00 Uhr bei jeder Witterung Dienstag und Donnerstag \n(Hauptsaison ab 06.00) \nab 16 Jahren",
            "preise": "Einzeleintritt:\r\nKinder CHF: 2.50 (6 bis 16 Jahre)\r\nKinder bis 6 Jahre in Begleitung: Gratis\r\nErwachsene: CHF 5.00\r\nLehrlinge/Studenten: CHF 4.00\r\n\r\nReduktion ab 17 Uhr: minus CHF 1.00\r\n\r\nGruppen, ab 10 Personen:\r\nKinder CHF 2.00\r\nErwachsene CHF 4.00\r\nLehrlinge/Studenten CHF 3.00\r\n\r\nSaisonabonnement:\r\nKinder CHF 50.00\r\nErwachsene CHF 80.00\r\nLehrlinge/Studenten CHF 65.00\r\nFamilie CHF 140.00\r\n\r\nSaison-Kabinen: Feste Kabine CHF 70.00\r\n\r\n10er-Abonnement:\r\nKinder CHF 22.50\r\nErwachsene CHF 45.00\r\nLehrlinge/Studenten CHF 36.00\r\nGästekarte CHF 4.00\r\n\r\nSaisonabonnementbesitzer erhalten in allen Appenzeller Freibäder gegen\r\nVorweisung des Abonnements eine Ermässigung von 50% auf Einzeleintritte.",
            "info": "Das 1932/33 von Ingenieur Beda Hefti erbaute Schwimm- und Sonnenbad Heiden\r\nfand damals als eines der modernsten der Schweiz weitherum Beachtung. Die\r\n1999 sorgfältig restaurierte Anlage ist fast vollständig im ursprünglichen\r\nZustand erhalten. Die schnörkellose Architektur und intensive Farbigkeit\r\nstehen für Hygiene und gesundheitsbewusste Lebensauffassung. Umfassende \r\nErneuerungen 2014 in der Schwimmbadtechnik bürgen weiterhin für bekannt gute\r\nWasserqualität.\r\n\r\nSportbecken 50 m x 16 m\r\nSprungturm 1m, 3m und 5m\r\nNichtschwimmerbecken 18m x 15 m\r\nSeparates Planschbecken mit Spielbach\r\nSpielwiese, Bouleplatz und Beachvolleyballfeld\r\nGrillstelle\r\nRestaurant (071 891 26 56, freier Eintritt) und Kiosk",
            "wetterort": "StGallen",
            "uv_station_name": "St. Gallen",
            "uv_wert": 1,
            "uv_date": "2015-02-22 00:00:00",
            "uv_date_pretty": "22.02.",

*/

    public class Weather {
    }
}
