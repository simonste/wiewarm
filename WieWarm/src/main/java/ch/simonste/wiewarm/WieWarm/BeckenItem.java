package ch.simonste.wiewarm.WieWarm;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import ch.simonste.wiewarm.R;


public class BeckenItem extends LinearLayout {

    public BeckenItem(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
    }

    public void setContent(BeckenInfo info) {
        ImageView image = (ImageView) findViewById(R.id.typimage);
        switch (info.typ) {
            case Fluss:
                image.setImageResource(R.drawable.fluss);
                break;
            case See:
                image.setImageResource(R.drawable.see);
                break;
            case Freibad:
                image.setImageResource(R.drawable.freibad);
                break;
            case Hallenbad:
                image.setImageResource(R.drawable.hallenbad);
                break;
        }
        ((TextView) findViewById(R.id.beckenname)).setText(info.beckenname + ":");
        ((TextView) findViewById(R.id.temperatur)).setText(info.temp + "°C");
    }
}
