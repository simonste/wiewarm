package ch.simonste.wiewarm.WieWarm;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Date;
import java.util.List;

import ch.simonste.wiewarm.FavoritesAdapter;
import ch.simonste.wiewarm.R;


public class BadItem extends LinearLayout {

    public BadItem(Context context) {
        super(context, null);
    }

    public BadItem(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
    }

    public void setInfo(BadInfo bad, final FavoritesAdapter favoritesAdapter) {
        TextView lastUpdate = (TextView) findViewById(R.id.lastupdated);

        CheckBox favorite = (CheckBox) findViewById(R.id.favorite);
        favorite.setTag(bad.id);
        favorite.setChecked(favoritesAdapter.isFavorite(bad.id));
        favorite.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (b)
                            favoritesAdapter.addFavorite((Integer) compoundButton.getTag());
                        else
                            favoritesAdapter.removeFavorite((Integer) compoundButton.getTag());
                    }
                }
        );

        List<BeckenInfo> beckenInfo = bad.becken;
        ((TextView) findViewById(R.id.badname)).setText(bad.badname + " " + bad.ort);

        LinearLayout becken = (LinearLayout) findViewById(R.id.becken);
        becken.removeAllViews();
        for (int n = 0; n < beckenInfo.size(); n++) {
            BeckenItem beckenItem = (BeckenItem) LayoutInflater.from(getContext()).inflate(R.layout.becken, null);
            beckenItem.setContent(beckenInfo.get(n));
            becken.addView(beckenItem);

            lastUpdate.setText(getLastUpdateTime(beckenInfo.get(n).date));
        }
    }

    private String getLastUpdateTime(Date date) {
        if (date == null) return getResources().getString(R.string.neverupdated);
        Date now = new Date();
        int days = (int) Math.floor((now.getTime() - date.getTime()) / (1000 * 60 * 60 * 24));
        int hours = (int) Math.floor((now.getTime() - date.getTime()) / (1000 * 60 * 60) - days * 24);

        String ret = getResources().getString(R.string.lastupdate);
        if (days > 0) ret += getResources().getQuantityString(R.plurals.days, days, days);
        if (days < 4)
            ret += getResources().getQuantityString(R.plurals.hours, hours, hours);
        return ret;
    }

}
