package ch.simonste.wiewarm;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import ch.simonste.wiewarm.WieWarm.BadInfo;


// TODO: clarify names, not bath and bad, baths baeder
public class WieWarmWrapper extends AsyncTask<String, Void, String> {
    private static final String TAG = "WieWarm";
    private static final String GET_TEMP = "TEMP";
    private static final String GET_BATH = "BATH";
    private static final String SEARCH_BATHS = "BATHS";
    WieWarmCaller caller;
    private String type;

    public void getTemperature(int bathid, WieWarmCaller caller) {
        this.caller = caller;
        type = GET_TEMP;
        get("https://www.wiewarm.ch/api/v1/temperature.json/" + bathid);
    }

    public void getBath(int bathid, WieWarmCaller caller) {
        this.caller = caller;
        type = GET_BATH;
        get("https://www.wiewarm.ch/api/v1/bad.json/" + bathid);
    }

    public void searchBaths(String pattern, WieWarmCaller caller) {
        this.caller = caller;
        type = SEARCH_BATHS;
        get("https://www.wiewarm.ch/api/v1/bad.json?search=" + pattern);
    }

    private void get(String url) {
        Log.d(TAG, url);
        execute(url);
    }

    private List<BadInfo> parseBadList(String json) throws JSONException {
        List<BadInfo> baeder = new ArrayList<>();
        JSONArray list = new JSONArray(json);
        for (int i = 0; i < list.length(); i++) {
            baeder.add(new BadInfo(list.getString(i)));
        }
        return baeder;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            URL url = new URL(strings[0]);
            final HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            if (urlConnection.getResponseCode() == 200) {
                Log.d(TAG, "Connection succeeded");
                try {
                    InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        result.append(line).append('\n');
                    }
                    Log.v(TAG, "Result: " + result.toString());
                    return result.toString();
                } finally {
                    urlConnection.disconnect();
                }
            }
        } catch (MalformedURLException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        if (result == null || result.length() == 0) {
            caller.gotBaeder(new ArrayList<BadInfo>());
            return;
        }
        try {
            switch (type) {
                case GET_BATH:
                    BadInfo bad = new BadInfo(result);
                    caller.gotBad(bad);
                    break;
                case SEARCH_BATHS:
                    List<BadInfo> baeder = parseBadList(result);
                    caller.gotBaeder(baeder);
                    break;
                default:
            }
        } catch (JSONException e) {
            Log.e(TAG, e.toString());
        }

    }

    public interface WieWarmCaller {
        void gotBad(BadInfo bad);

        void gotBaeder(List<BadInfo> baeder);
    }
}
