package ch.simonste.wiewarm;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import java.util.List;

import ch.simonste.wiewarm.WieWarm.BadInfo;
import ch.simonste.wiewarm.WieWarm.BeckenInfo;
import ch.simonste.wiewarm.WieWarm.BeckenItem;


public class DetailsFragment extends Fragment implements WieWarmWrapper.WieWarmCaller {

    private View mRootview;
    private TextView mNews;
    private TextView mPreise;
    private TextView mZeiten;
    private TextView mInfos;
    private LayoutInflater mInflater;
    private DisplayImageOptions mOptions;

    public DetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024)
                .memoryCacheSizePercentage(13) // default
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .build();

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);

        mOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        mInflater = LayoutInflater.from(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootview = inflater.inflate(R.layout.fragment_details, container, false);

        mNews = (TextView) mRootview.findViewById(R.id.news);
        mPreise = (TextView) mRootview.findViewById(R.id.preise);
        mZeiten = (TextView) mRootview.findViewById(R.id.zeiten);
        mInfos = (TextView) mRootview.findViewById(R.id.info);
        return mRootview;
    }

    public void showDetails(BadInfo bad) {
        if (bad.email == null) {
            new WieWarmWrapper().getBath(bad.id, this);
        }
        showInfos(bad);
    }

    public void showDetails(int id) {
        new WieWarmWrapper().getBath(id, this);
    }

    @Override
    public void gotBad(BadInfo bad) {
        showInfos(bad);
    }

    @Override
    public void gotBaeder(List<BadInfo> baeder) {

    }

    private void showInfos(BadInfo bad) {
        mRootview.setVisibility(View.VISIBLE);
        ((TextView) mRootview.findViewById(R.id.name)).setText(bad.badname + " " + bad.ort);

        LinearLayout gallery = mRootview.findViewById(R.id.gallery);
        gallery.removeAllViews();
        for (final BadInfo.Picture picture : bad.pictures) {
            final ImageView imageView = (ImageView) mInflater.inflate(R.layout.gallery_item, gallery, false);
            gallery.addView(imageView);
            ImageLoader.getInstance().displayImage(picture.url, imageView, mOptions);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ImageDialog fragment = ImageDialog.newInstance(imageView.getDrawable(), picture.description);
                    fragment.show(getFragmentManager(), null);
                }
            });
        }
        List<BeckenInfo> beckenInfo = bad.becken;
        LinearLayout becken = mRootview.findViewById(R.id.becken);
        becken.removeAllViews();
        for (int n = 0; n < beckenInfo.size(); n++) {
            BeckenItem beckenItem = (BeckenItem) LayoutInflater.from(getActivity()).inflate(R.layout.becken, null);
            beckenItem.setContent(beckenInfo.get(n));
            becken.addView(beckenItem);
        }

        if (!bad.news.isEmpty()) {
            mNews.setText(bad.news.get(0).text);
        } else {
            mNews.setText("");
        }
        mPreise.setText(bad.preise);
        mZeiten.setText(bad.zeiten);
        if (bad.info != null) {
            mInfos.setText(Html.fromHtml(bad.info));
        } else {
            mInfos.setText("");
        }

    }
}
