package ch.simonste.wiewarm;

import android.app.DialogFragment;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageDialog extends DialogFragment {
    static Drawable image;

    static ImageDialog newInstance(Drawable image, String description) {
        ImageDialog id = new ImageDialog();

        ImageDialog.image = image;

        Bundle args = new Bundle();
        args.putString("description", description);
        id.setArguments(args);

        return id;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Light_NoTitleBar);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.dialog_image, container, false);

        String description = getArguments().getString("description");
        if (description != null && !description.isEmpty()) {
            ((TextView) v.findViewById(R.id.text)).setText(description);
        }
        ((ImageView) v.findViewById(R.id.image)).setImageDrawable(image);
        return v;
    }
}
