package ch.simonste.wiewarm;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.SearchView;

import ch.simonste.wiewarm.WieWarm.BadInfo;


public class MainActivity extends Activity implements ListFragment.ListFramentListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ListFragment listFragment = (ListFragment) getFragmentManager()
                .findFragmentById(R.id.list);
        listFragment.loadFavorites();
    }

    public void showDetails(BadInfo bad) {
        DetailsFragment detailsFragment = (DetailsFragment) getFragmentManager()
                .findFragmentById(R.id.details);
        if (detailsFragment != null) {
            detailsFragment.showDetails(bad);
        } else {
            Intent intent = new Intent(getApplicationContext(), DetailsActivity.class);
            intent.putExtra(DetailsActivity.ARG_ID, bad.id);
            startActivity(intent);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);

            ListFragment listFragment = (ListFragment) getFragmentManager()
                    .findFragmentById(R.id.list);
            listFragment.searchBaths(query);
        }
    }
}
