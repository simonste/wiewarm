package ch.simonste.wiewarm;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import ch.simonste.wiewarm.WieWarm.BadInfo;

public class DetailsActivity extends Activity implements ListFragment.ListFramentListener {
    public static final String ARG_ID = "id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getResources().getConfiguration().screenLayout == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            // we can show the dialog in-line with the list so we don't need this activity.
            finish();
            return;
        }

        if (savedInstanceState == null) {
            setContentView(R.layout.activity_details);
            // During initial setup, plug in the details fragment.
            DetailsFragment details = new DetailsFragment();
            details.setArguments(getIntent().getExtras());

            onNewIntent(getIntent());
        }
    }

    public void showDetails(BadInfo bad) {
        DetailsFragment detailsFragment = (DetailsFragment) getFragmentManager()
                .findFragmentById(R.id.details);
        if (detailsFragment != null) {
            detailsFragment.showDetails(bad);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        int id = intent.getExtras().getInt(ARG_ID);
        DetailsFragment detailsFragment = (DetailsFragment) getFragmentManager()
                .findFragmentById(R.id.details);
        if (detailsFragment != null) {
            detailsFragment.showDetails(id);
        }
    }
}
