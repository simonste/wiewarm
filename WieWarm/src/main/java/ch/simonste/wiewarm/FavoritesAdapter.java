package ch.simonste.wiewarm;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import ch.simonste.wiewarm.WieWarm.BadInfo;
import ch.simonste.wiewarm.WieWarm.BadItem;

public class FavoritesAdapter extends ArrayAdapter<BadInfo> {
    public static final String PREFS_NAME = "WieWarmFavorites";

    private final Context mContext;
    private final Comparator<BadInfo> mComparator;
    private SharedPreferences mPreferences;
    private List<Integer> mFavIDs = new ArrayList<>();

    public FavoritesAdapter(Context context, List<BadInfo> baeder) {
        super(context, R.layout.bad_item, baeder);
        this.mContext = context;
        this.mComparator = new Comparator<BadInfo>() {
            public int compare(BadInfo bad1, BadInfo bad2) {
                String b1 = bad1.badname + " " + bad1.ort;
                String b2 = bad2.badname + " " + bad2.ort;
                return b1.compareTo(b2);
            }
        };
        mPreferences = context.getSharedPreferences(FavoritesAdapter.PREFS_NAME, Context.MODE_PRIVATE);

        String favorites = mPreferences.getString("Favorites", "");
        String[] favs = favorites.split(",");
        mFavIDs = new ArrayList<>();

        for (String fav : favs) {
            try {
                mFavIDs.add(Integer.parseInt(fav.trim()));
            } catch (NumberFormatException e) {
                return;
            }
        }
    }

    public boolean isFavorite(int id) {
        return mFavIDs.contains(id);
    }

    public void addFavorite(int id) {
        String favorites = mPreferences.getString("Favorites", "");
        if (favorites.length() > 0) favorites += ",";
        favorites += id;
        mFavIDs.add(id);
        saveFavorites(favorites);
    }

    public void removeFavorite(int id) {
        String favorites = mPreferences.getString("Favorites", "");
        String[] favs = favorites.split(",");

        mFavIDs.clear();
        favorites = "";
        for (String fav : favs) {
            try {
                int curId = Integer.parseInt(fav.trim());
                if (curId != id) {
                    mFavIDs.add(curId);
                    if (favorites.length() > 0) favorites += ",";
                    favorites += curId;
                }
            } catch (NumberFormatException ignored) {
            }
        }
        saveFavorites(favorites);
    }

    public List<Integer> getFavorites() {
        return mFavIDs;
    }

    private void saveFavorites(String favorites) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString("Favorites", favorites);
        editor.apply();
    }

    public void sort() {
        sort(mComparator);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.bad_item, null, false);

            ((BadItem) convertView).setInfo(getItem(position), this);
        } else {
            ((BadItem) convertView).setInfo(getItem(position), this);
        }

        return convertView;
    }
}
