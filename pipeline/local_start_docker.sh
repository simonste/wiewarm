#!/bin/bash

DIR=$PWD/$(dirname "$0")/..
docker run -it -u root --volume=$DIR:/work --workdir="/work" --memory=4g -t mingc/android-build-box:latest
