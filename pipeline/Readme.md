# Run pipeline local
## Install docker
```
sudo snap install docker
```

Allow running without sudo:
https://docs.docker.com/install/linux/linux-postinstall

## Download latest image
```
docker pull mingc/android-build-box:latest
```

## Start docker
```
docker run -it -u root --volume=$PWD/$(dirname "$0")/..:/work --workdir="/work" --memory=4g -t mingc/android-build-box:latest
```
